'use strict'
const
    express     = require('express'),
    router      = express.Router(),
    httpError   = require('throw.js'),
    {appError}  = require('../bin/config.js'),
    request     = require('request'), rand = Math.floor(Math.random()*100000000).toString();


router.post('/getlistingsbyGeo', (req, res, next) => {
    
    //Allowing case insensitive fieldnames
    var body = req.body;

    var params = ["City", "State", "Zip", "MonthsBack", "EffectiveDate", "imagesON", "MinBed", "MaxBed", "MinBath",
                "MaxBath", "MinYearBuilt", "MaxYearBuilt", "MinArea", "MaxArea", "MinLotSize", "MaxLotSize", "MinPrice",
                "MaxPrice", "DOM", "MlsStatus", "PageSize", "PageNumber", "OrderId", 
                "UnitNumber", "Rental", "PropertyType", "PropertySubType"];
    
    var key, keys = Object.keys(body);
    var n = keys.length;
    var lowerCasedBody={}

    while (n--) {
        key = keys[n];
        lowerCasedBody[key.toLowerCase()] = body[key];
    }
    
    req.body = {};
    params.forEach( (para) => {
        
        if( lowerCasedBody[ para.toLowerCase() ] )
            req.body[ para ] = lowerCasedBody[ para.toLowerCase() ];
    } )

    var fieldNames = Object.keys(req.body);
    var fieldValues = Object.values(req.body);
    
    if(typeof req.body.City === "undefined" && typeof req.body.Zip === "undefined" && typeof req.body.FipsCode === "undefined" && typeof req.body.Country === "undefined" && typeof req.body.State === "undefined" )
        return res.status( 400 ).send( "Specify Zip or City or FipsCode or Country or State " );

    //composing the url
    var url = "https://staging-api.propmix.io/mlslite/val/v1/GetListingsByGeo?" + "access_token=" + process.env.SERVICE_MLS_PROPMIX_ACCESSTOKEN;
    
    for(var i = 0 ; i < fieldNames.length ; i++){
        url = url + "&" + fieldNames[i] + "=" + fieldValues[i];
    }

    //send the get request
    request.get( url, function (err, resp, body) {
     
        var response = JSON.parse( body.toString() );

        if(typeof response.ErrorCode !== "undefined")
            res.status(response.ErrorCode).send(response); 
        
        else
            res.status(200).send( response);      

    });

   
});


router.post('/getlistingsbyradius', (req, res, next) => {

    //Allowing case insensitive fieldnames
    var body = req.body;

    var params = ["Street", "City", "State", "Radius", "Zip", "isZipON", "imagesON", "MinBed", "Latitude", "Longitude",
                "MonthsBack", "EffectiveDate","MlsStatus", "PageNumber", "PageSize", "PropertyType", "OrderId",
                "Rental", "PropertyType", "PropertySubType"];
    
    
    var key, keys = Object.keys(body);
    var n = keys.length;
    var lowerCasedBody={}

    while (n--) {
        key = keys[n];
        lowerCasedBody[key.toLowerCase()] = body[key];
    }
   
    req.body = {};
    params.forEach( (para) => {
        
        if( lowerCasedBody[ para.toLowerCase() ] )
            req.body[ para ] = lowerCasedBody[ para.toLowerCase() ];
    } )

    var fieldNames = Object.keys(req.body);
    var fieldValues = Object.values(req.body);

    if(typeof req.body.Street === "undefined" && typeof req.body.City === "undefined" && typeof req.body.State === "undefined" && typeof req.body.Latitude === "undefined" && typeof req.body.Longitude === "undefined")
        return res.status( 400 ).send( "Specify Street or City & State or Latitude & Longitude" );

    //composing the url
    var url = "https://staging-api.propmix.io/mlslite/val/v1/GetListingsByRadius?access_token=" + process.env.SERVICE_MLS_PROPMIX_ACCESSTOKEN; 
 
    for(var i = 0 ; i < fieldNames.length ; i++){
        url = url + "&" + fieldNames[i] + "=" + fieldValues[i];
    }
     
    //send the get request
    request.get( url , function (err, resp, body) {
        var response = JSON.parse( body.toString() );

        if(typeof response.ErrorCode !== "undefined")
            res.status(response.ErrorCode).send(response); 

        else
            res.status(200).send( response);   

    });
   
});

router.post('/getcountbygeo', (req, res, next) => {

    var fieldNames = Object.keys(req.body);
    var fieldValues = Object.values(req.body);

    if(typeof req.body.Zip === "undefined") 
        if(typeof req.body.FipsCode === "undefined")
            if(typeof req.body.Country === "undefined" && typeof req.body.State === "undefined")
                if(typeof req.body.City === "undefined" && typeof req.body.State === "undefined")
                    return res.status( 400 ).send( "Specify Street or City & State or Latitude & Longitude" );

    //composing the url
    var url = "https://staging-api.propmix.io/mlslite/val/v1/GetCountByGeo?access_token=" + process.env.SERVICE_MLS_PROPMIX_ACCESSTOKEN; 
    
    for(var i = 0 ; i < fieldNames.length ; i++){
        url = url + "&" + fieldNames[i] + "=" + fieldValues[i];
    }
     
    //send the get request
    request.get( url , function (err, resp, body) {
        
        var response = JSON.parse( body.toString() );

        if(typeof response.ErrorCode !== "undefined")
            res.status(response.ErrorCode).send(response); 

        else
            res.status(200).send( response);   
       
    });

})

router.post('/getcountbyradius', (req, res, next) => {

    var fieldNames = Object.keys(req.body);
    var fieldValues = Object.values(req.body);

    if(typeof req.body.Street === "undefined")  
        if(typeof req.body.Radius === "undefined")
            if(typeof req.body.City === "undefined" && typeof req.body.State === "undefined")
                if(typeof req.body.Latitude === "undefined", typeof req.body.Longitude === "undefined")
                    return res.status( 400 ).send( "Specify Street or City & State or Latitude & Longitude" );

    //composing the url
    var url = "https://staging-api.propmix.io/mlslite/val/v1/GetCountByRadius?access_token=" + process.env.SERVICE_MLS_PROPMIX_ACCESSTOKEN; 
    
    for(var i = 0 ; i < fieldNames.length ; i++){
        url = url + "&" + fieldNames[i] + "=" + fieldValues[i];
    }
     
    //send the get request
    request.get( url , function (err, resp, body) {

        var response = JSON.parse( body.toString() );

        if(typeof response.ErrorCode !== "undefined")
            res.status(response.ErrorCode).send(response); 

        else
            res.status(200).send( response);   

    });

})

router.post('/GetImageNamesByListingID', (req, res, next) => {

    let ListingId   = req.body.ListingId || '';
    let AccessToken = req.body.AccessToken || '';
    let OrderId     = req.body.OrderId || '';

    var url = 'https://api.propmix.io/mlslite/val/v1/GetImageNamesByListingID' + '/?'
            + 'OrderId=' + OrderId + '&ListingId=' + ListingId + '&AccessToken=' + AccessToken;


    //send the get request
    request.get( url , function (err, resp, body) {
        if (err) 
            res.status( 400 ).send( "An error has been occured" );

        res.status( 200 ).send( JSON.parse( body.toString() ) );   

    });
});

router.post('/GetImageNamesByAddress', (req, res, next) => {

    let Street      = req.body.Street || '';
    let Zip         = req.body.Zip || '';
    let City        = req.body.City || '';
    let State       = req.body.State || '';
    let Width       = req.body.Width || '';
    let Height      = req.body.Height || '';
    let AccessToken = req.body.AccessToken || '';
    let OrderId     = req.body.OrderId || '';

    var url = 'https://api.propmix.io/mlslite/idx/v1/GetImageNamesByAddress' + '/?'
            + 'Street=' + Street + '&Zip=' + Zip + '&City=' + City + '&State' + State 
            + '&Width=' + Width + '&Height' + Height
            + '&OrderId=' + OrderId
            + '&AccessToken' + AccessToken;

    console.log(url);
    //send the get request
    request.get( url , function (err, resp, body) {
        if (err) 
            res.status( 400 ).send( "An error has been occured" );

        res.status( 200 ).send( JSON.parse( body.toString() ) );   

    });
});

router.post('/GetListingHistory', (req, res, next) => {

    let ListingId = req.body.ListingId || '';
    let MonthsBack = req.body.MonthsBack || '';
    let AccessToken = req.body.AccessToken || '';

    var url = 'https://api.propmix.io/mlslite/idx/v1/GetListingHistory' + '/?'
            + 'ListingId=' + ListingId + '&MonthsBack=' + MonthsBack
            + '&AccessToken=' + AccessToken;
})

module.exports = router;
