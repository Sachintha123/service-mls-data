# service-mls-data

curl -X POST \
  http://localhost:8081/getlistingsbygeo \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d286dd57-3f87-486d-ac40-23cb033ae2dd' \
  -d '{
	"City" : "Avondale",
	"Zip" : "85323",
	"MonthsBack"  : 10,
	"MinPrice" : 1000,
	"OrderId" : 45
}'



curl -X POST \
  http://localhost:8081/getListingsbyradius \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 484d548f-fc9b-4925-ab1e-7507abc5ae8a' \
  -H 'cache-control: no-cache' \
  -d '{
	"Street" : "3565 N Nevada St",
	"City" : "Kingman",
	"State" : "Nevada",
	"Latitude" : 35.22677200,
	"Longitude" : -114.01234400,
	"Radius" : 5,
	"MonthsBack" : 10
}'


curl -X POST \
  http://localhost:8081/getcountbygeo \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: a4a8b108-deed-4421-acde-81568cc714eb' \
  -H 'cache-control: no-cache' \
  -d '{
	"City" : "Avondale",
	"Zip" : "85323",
	"MinPrice" : 1000,
	"MonthsBack" : 10
}'

curl -X POST \
  http://localhost:8081/getcountbyradius \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 0b5542e3-db74-4b73-beb3-a1de0b8eb314' \
  -H 'cache-control: no-cache' \
  -d '{
	"Street" : "3565 N Nevada St",
	"City" : "Kingman",
	"State" : "Nevada",
	"Latitude" : 35.22677200,
	"Longitude" : -114.01234400,
	"Radius" : 5,
	"MonthsBack" : 10
}'